package com.dev.circlecylinderapi.circle_cylinder_api.controllers;
import com.dev.circlecylinderapi.circle_cylinder_api.Circle;
import com.dev.circlecylinderapi.circle_cylinder_api.Cylinder;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
// import org.springframework.web.bind.annotation.RequestHeader;
// import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleCylinderController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea() {
        Circle circle1 = new Circle(2.0);
        System.out.println(circle1);
        return circle1.getArea();
    }
    @CrossOrigin
    @GetMapping("/cylinder-volume")
    public double getCylinderVolume() {
        Cylinder cylinder1 = new Cylinder(2.0, 3.0);
        System.out.println(cylinder1);
        return cylinder1.getVolume();
    }
}
