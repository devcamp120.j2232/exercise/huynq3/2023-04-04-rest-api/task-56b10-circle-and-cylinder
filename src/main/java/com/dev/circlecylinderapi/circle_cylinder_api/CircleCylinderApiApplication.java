package com.dev.circlecylinderapi.circle_cylinder_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircleCylinderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircleCylinderApiApplication.class, args);
	}

}
